package com.ithuameng.netty.ch7;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

/**
 * page级别的内存分配 allocateNormal()
 * subpage级别的内存分配 allocateTiny()
 *
 * @author xuc
 * @date 2022-11-07
 */
public class PooledByteBufAllocatorTest {
    public static void main(String[] args) {
        int page = 1024 * 8;
        PooledByteBufAllocator allocator = PooledByteBufAllocator.DEFAULT;
        allocator.directBuffer(2 * page);
        ByteBuf byteBuf = allocator.directBuffer(16);
        byteBuf.release();
    }
}
