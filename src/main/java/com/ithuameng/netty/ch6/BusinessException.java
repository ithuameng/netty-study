package com.ithuameng.netty.ch6;

public class BusinessException extends Exception {

    public BusinessException(String message) {
        super(message);
    }
}
